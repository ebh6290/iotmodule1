**What is IoT?**

_The Internet of things (IoT) is a system of interrelated computing devices, mechanical and digital machines provided with unique identifiers (UIDs) and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction._


**Some Examples of Iot:**


_Google Home Voice Controller_

_Amazon Echo Plus Voice Controller_

_Wearable health monitors_

_Footbot Air Quality Monitor_

_industrial Autonomous farming equipment._


![](https://www.techprate.com/wp-content/uploads/2020/01/internet-of-things.jpg)

**What is industrial ioT(IIoT)?**


_IIoT stands for the Industrial Internet of Things or Industrial IoT that initially mainly referred to an industrial framework whereby a large number of devices or machines are connected and synchronized through the use of software tools and third platform technologies in a machine-to-machine and Internet of Things context, later an Industry 4.0 or Industrial Internet context._


![](https://miro.medium.com/max/1000/1*KHJ8UBLysEk5LbYdeNTWEw.jpeg)

**Industrial Revolution:**


- Industry 1.0 (1784):

  _Mechanisation, Steam power & Weaving loom._

- Industry 2.0 (1870):
  
  _Mass Production, Assembly line & Electrical energy._

- Industry 3.0 (1969):

   _Automation, Computers & Electronics._
  
  _Data are stored in databases and represented in excels._


- Industry 4.0 (Today):

   _Heavy usage of Internet._
   
   _Cyber Physical Systems, Internet of Things and Networking_


![](https://img.automationworld.com/files/base/pmmi/all/image/2018/08/aw_270171_industry40.png?ar=16%3A9&auto=format&crop=focalpoint&fit=crop&fp-x=0.5&fp-y=0.5&w=1140)

**Industry 4.0:**

_Industry 4.0 is Industry 3.0 connected to Internet, which is called IoT. Connecting to Internet makes data communication faster, easier without data loss._

**Architecture of Industry 4.0:**

![](https://i.imgur.com/zHb1J8U.png)

**Some Problems in industry 4.0:**

  _Cost_

  _Downtime_

 _Reliability_

**Solution:**

_Get data from Industry 3.0 devices/meters/sensors without changes to the original device. Send the data to the Cloud using Industry 4.0 devices_

**Industry 3.0:**

_Now industry 3.0 was a huge leap ahead wherein the advent of computer and automation ruled the industrial scene. It was during this period of transformation era where more and more robots were used in to the processes to perform the tasks which were performed by humans. E.g. Use of Programmable Logic Controllers, Robots etc._

_Industry 3.0 use Sensors, PLC's, SCADA and ERP.
Workflow: Sensors --> PLC --> SCADA & ERP._

**Architecture of Industry 3.0:**

![](https://i.imgur.com/Fw3SJVh.png)

**Difference between Industry 4.0 and Industry 3.0:**

_To put in simple words the basic difference being that the machines work autonomously without the intervention of a human in Industry 4.0. Whereas in the industry 3.0 the machines are only automatized. For example: Consider a CNC Milling machine, if the machine is in the era of Industry 3.0, the tool changes can be done automatically but the speed at which the spindle should run is to be observed by the operator and the corrections should be made by him._

_Similarly if the machine is in the Industrie 4.0 the tool changes are automatic at the same time the spindle speeds and all other parameters essential to carry out the process are recorded by the hundreds of sensors present in the machine and the optimum settings are done on its own based on the large amount of data there is to compare and optimize the process._

**Steps to make an IIoT Product.**
 
 _Identify most popular Industry 3.0 devices._
 
 _Study Protocols that these devices Communicate._
 
 _Get data from the Industry 3.0 devices._
 
 _Send the data to cloud for Industry 4.0._

**IoT Platforms:**

_IoT platforms helps to analyze the data, predective maintainence._

**Examples are as follows:**

_IBM Watson IoT Platform_

_Google Cloud IOT_

_Amazon Web Services IOT_

_Microsoft Azure IOT_

_Matlab Thingspeak IOT_
